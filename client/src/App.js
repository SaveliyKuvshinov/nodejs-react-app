import React, {useState} from "react";
import logo from './logo.svg';
import './App.css';

function App() {
  const [response, setResponse] = useState(null);

  const handler = () => {
    fetch('/works')
      .then((result) => {
        return result.json()
      })
      .then((result) => {
        console.log(result)
        setResponse(result);
      })
      .catch((error) => {
        console.error(error);
      })
  }

  return (
    <div className="App">
      <button onClick={handler}>
        send request
      </button>
      {response && 
        <p>{response.message}</p>
      }
    </div>
  );
}

export default App;
